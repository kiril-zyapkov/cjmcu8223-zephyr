cmake_minimum_required(VERSION 3.20.0)

set(BOARD nrf51_cjmcu8223)

set(BOARD_ROOT ${CMAKE_CURRENT_SOURCE_DIR})
# list(APPEND BOARD_ROOT ${CMAKE_CURRENT_SOURCE_DIR})

find_package(Zephyr REQUIRED HINTS $ENV{ZEPHYR_BASE})

project(cjmcu8223_sample)

target_sources(app PRIVATE src/main.c)