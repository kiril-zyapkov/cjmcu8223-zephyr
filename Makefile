.PHONY: app bootloader flash-app

BOARD ?= nrf51_cjmcu8223
BOARD_ROOT ?= $(CURDIR)

MCUBOOT_BUILD_DIR = build_mcuboot
APP_BUILD_DIR = build_app


MCUBOOT_DIR = ../bootloader/mcuboot
BOOT_HEADER_LEN = 0x200
APP_SLOT_SIZE = 0x1a000
FLASH_ALIGNMENT = 8
BOOT_VERSION = 1.2

IMGTOOL = $(MCUBOOT_DIR)/scripts/imgtool.py
ASSEMBLE = $(MCUBOOT_DIR)/scripts/assemble.py


bootloader:
	west build -b $(BOARD) -p auto -d $(MCUBOOT_BUILD_DIR) \
		$(MCUBOOT_DIR)/boot/zephyr -- \
		-DBOARD_ROOT=$(BOARD_ROOT) \
		-DOVERLAY_CONFIG=$(CURDIR)/mcuboot-overlay.conf


app:
	west build -b $(BOARD) -p auto -d $(APP_BUILD_DIR) \
		$(CURDIR) -- \
		-DBOARD_ROOT=$(BOARD_ROOT) \
		-DCONFIG_BOOTLOADER_MCUBOOT=y

	$(IMGTOOL) create \
		--align $(FLASH_ALIGNMENT) \
		--header-size $(BOOT_HEADER_LEN) \
		--slot-size $(APP_SLOT_SIZE) \
		--version $(BOOT_VERSION) \
		$(APP_BUILD_DIR)/zephyr/zephyr.hex $(APP_BUILD_DIR)/app.hex

full:
	$(ASSEMBLE) --help

flash-app:
	arm-none-eabi-gdb -x gdb-bmp-connect.scr \
		--batch \
		-ex "load $(APP_BUILD_DIR)/app.hex" \
		-ex "kill"

flash-bootloader:
	ninja -C $(MCUBOOT_BUILD_DIR) flash

clean:
	@rm -rf build_*