# SPDX-License-Identifier: Apache-2.0

if(CONFIG_LIS3DH)
  zephyr_library()
  zephyr_library_sources(board.c)
endif()
