/*
 * Copyright (c) 2018 Kiril Zyapkov
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <device.h>
#include <drivers/gpio.h>
#include <init.h>
#include <kernel.h>

#include <soc.h>

#define LIS3DH_SCL_GPIO_PIN	7
#define LIS3DH_SDA_GPIO_PIN	6
#define LIS3DH_SA0_GPIO_PIN	4
#define LIS3DH_CS_GPIO_PIN	3
#define LIS3DH_INT1_GPIO_PIN	1
#define LIS3DH_INT2_GPIO_PIN	2
#define LIS3DH_GPIO_PORT        CONFIG_GPIO_NRF5_P0_DEV_NAME


int cjmcu8332_pinmux_init(const struct device *dev) {
	struct device *gpio = device_get_binding(LIS3DH_GPIO_PORT);

	/* when using the I2C, CS must be tied high. */
	gpio_pin_configure(gpio, LIS3DH_CS_GPIO_PIN, GPIO_DIR_OUT);
	gpio_pin_write(gpio, LIS3DH_CS_GPIO_PIN, 1);

	/* SA0 has pullup enabled by default so it's better to keep it high,
	this sets the sensor address 0x19 */
	gpio_pin_configure(gpio, LIS3DH_SA0_GPIO_PIN, GPIO_DIR_OUT);
	gpio_pin_write(gpio, LIS3DH_SA0_GPIO_PIN, 1);

	return 0;
}

SYS_INIT(cjmcu8332_pinmux_init, PRE_KERNEL_1,
	 CONFIG_KERNEL_INIT_PRIORITY_DEFAULT);
